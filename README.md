# UOC – Máster en diseño y desarrollo de Videojuegos 2018
# Jaume Castells Carles
# Practica
# Código reaprovechado de PEC3

## Nombre
**Metal Slug Tribute**

## Descripción
Clone del videojuego **Metal Slug** (SNK)

## Video
Video at /Video

## Nota
*A media práctica he tenido que replantear el proyecto por falta de tiempo y
me he centrado más en el apartado gráfico y presentacion, teniendo que dejar algo de lado
otras características ya presentadas en prácticas anteriores.

Además, en esta práctica hay dos añadidos importantes:

* El arma principal está hecha con una fuente de partículas
con colisiones en vez de proyectiles game object para diferenciar de otras
prácticas hechas anteriormente.*

* La segunda misión es un final boss. 

## Arte
Todo el arte es propiedad de SNK.

## Builds
Builds available:
### Android
Default target
### Windows
Path Builds/Win
### Linux
### Mac

## Programación
El objeto principal es el **GameManager**. Se encarga de crear y mantener la partida del jugador (estadísticas y info), cambiar de nivel, y de crear el manager de sonido además de otros cometidos generales.

Además del manager de juego también existen otros con sus propios cometidos:
* **StageManager**; se encarga de crear y mantener la pantalla actual. Está pensado para usarse de modo local en cada escena (nivel) con sus propiedades. Hay tantos StageManager como niveles.
* **SoundManager**; encargado del sonido. Sólo hay uno por partida ya que todo el tema sonido está centralizado en él. Contiene varias fuentes de sonido que se encarga de administrar.
* **UIManager**; encargado de la interfaz de usuario. Cuida tanto de los menús como del hud del jugador.

### Otras clases importantes
#### PlayerBehavior
Clase encargada del funcionamiento del player humano.

#### CameraBehavior
Clase base abstracta para el desarrollo de las camaras de juego.

#### MissionCamera
Sistema de cámara con las peculiaridades y el comportamiento de la cámara del videojuego para NeoGeo Metal Slug.

**Mucha más información en el código de los scripts en forma de comentarios y sumarios.**

 
