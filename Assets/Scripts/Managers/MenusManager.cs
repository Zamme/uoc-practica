﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenusManager : MonoBehaviour
{
    private GameObject pressAnyKeyText;
    private GameObject buttonsPanel;
    private GameObject optionsPanel;

    public enum MenuState { PressKey, Buttons}
    private MenuState menuState;

    private void Awake()
    {
        pressAnyKeyText = GameObject.Find("PressAnyKeyText");
        buttonsPanel = GameObject.Find("ButtonsPanel");
        optionsPanel = GameObject.Find("OptionsPanel");
    }
    void Start ()
    {
        pressAnyKeyText.SetActive(true);
        buttonsPanel.SetActive(false);
        optionsPanel.SetActive(false);
        menuState = MenuState.PressKey;
	}
	
    public void LoadOptionsMenu (bool show)
    {
        buttonsPanel.SetActive(!show);
        optionsPanel.SetActive(show);
    }

    public void StartGame ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame ()
    {
        Application.Quit();
    }

    private void Update()
    {
        if (menuState == MenuState.PressKey)
        {
            if (Input.anyKeyDown)
            {
                pressAnyKeyText.SetActive(false);
                buttonsPanel.SetActive(true);
                menuState = MenuState.Buttons;
            }
        }
    }
}
