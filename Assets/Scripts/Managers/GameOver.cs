﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    public Text resultText;

    private void Start()
    {
        string result;

        if (GlobalRefs.currentGameManager.wonLastMatch)
        {
            result = "You Won " + GlobalRefs.currentGameManager.player.currentStageScore + " points";
        }
        else
        {
            result = "You Lost";
        }
        resultText.text = result;
    }
    public void Quit ()
    {
        Application.Quit();
    }

    public void Restart ()
    {
        SceneManager.LoadScene(0);
    }
}
