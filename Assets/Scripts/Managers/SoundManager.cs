﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public int nAudioSources; // Min = 2 to avoid music overriding

    public AudioClip jumpAudioClip;
    public AudioClip pistolShootAudioClip;
    public AudioClip killAudioClip;
    public AudioClip bumpAudioClip;
    public AudioClip gameOver;
    public AudioClip stageClear;
    public AudioClip playerDie;
    public AudioClip kickEnemy;

    public enum Sounds {Jump, PistolShoot, Money, Kill, SuperMario, BreakBlock, Damage, PowerUpAppears, GameOver, StageClear, PlayerDie, KickEnemy, FlagPole, PowerUp}
    public enum Musics { Boss, FinalProduction, GameOver, MissionClear, OperationExplanationRanking, Stage_00, Stage_01, StageClear}

    private AudioSource[] audioSources;
    private int lastUsed = -1;
    private int lastPlaying;
    private int iMusicAudioSource = -1;

    private void Awake()
    {
        GlobalRefs.currentSoundManager = this;

        InitAudioSources();
    }

    void Start ()
    {
        //DontDestroyOnLoad(gameObject);
	}
	
    AudioSource GetNextAudioSource ()
    {
        lastPlaying = lastUsed;
        GetFreeAudioSourceIndex();

        return audioSources[lastUsed];
    }

    void InitAudioSources ()
    {
        if (nAudioSources < 2)
        {
            Debug.LogError("Audio sources number must be 2 minimum!");
            return;
        }

        audioSources = new AudioSource[nAudioSources];

        for (int a = 0; a < nAudioSources; a++)
        {
            audioSources[a] = gameObject.AddComponent<AudioSource>();
        }
    }

    void GetFreeAudioSourceIndex ()
    {
        GetNextAudioSourceIndex();

        while ((audioSources[lastUsed].isPlaying) || (lastUsed == lastPlaying) || (lastUsed == iMusicAudioSource))
        {
            GetNextAudioSourceIndex();
        }
    }

    void GetNextAudioSourceIndex ()
    {
        lastUsed++;

        if (lastUsed == audioSources.Length)
        {
            lastUsed = 0;
        }

    }

    public void PauseMusic (bool pause)
    {
        if (pause)
        {
            audioSources[iMusicAudioSource].Pause();
        }
        else
        {
            audioSources[iMusicAudioSource].UnPause();
        }
    }

    public int PlayAudioClip (AudioClip audioClip, bool withLoop = false)
    {
        AudioSource audioSource = GetNextAudioSource();
        audioSource.clip = audioClip;
        audioSource.loop = withLoop;
        audioSource.Play();

        return lastUsed;
    }

    public void PlayMusic (AudioClip audioClip)
    {
        if (iMusicAudioSource < 0)
        {
            iMusicAudioSource = PlayAudioClip(audioClip, true);
        }
        else
        {
            PauseMusic(false);
        }

    }

    /// <summary>
    /// TODO : Transform it to a Dict!
    /// </summary>
    /// <param name="sound"></param>
    public void PlaySound (Sounds sound, bool withLoop = false)
    {
        switch (sound)
        {
            case Sounds.PistolShoot:
                PlayAudioClip(pistolShootAudioClip, withLoop);
                break;
            case Sounds.Jump:
                PlayAudioClip(jumpAudioClip);
                break;
            case Sounds.Kill:
                PlayAudioClip(killAudioClip);
                break;
            case Sounds.Damage:
                PlayAudioClip(bumpAudioClip);
                break;
            case Sounds.GameOver:
                PlayAudioClip(gameOver);
                break;
            case Sounds.StageClear:
                PlayAudioClip(stageClear);
                break;
            case Sounds.PlayerDie:
                PlayAudioClip(playerDie);
                break;
            case Sounds.KickEnemy:
                PlayAudioClip(kickEnemy);
                break;
        }
    }

}
