﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private GameObject hud;

    private Text scoreText;
    //private Text livesText;
    private Text timeText;
    //private Text armsText;
    //private Text bombText;

    private GameObject startPage;

    private void Awake()
    {
        GlobalRefs.currentUIManager = this;
        GetUIRefs();
    }

    void Start ()
    {
    }

    void GetUIRefs()
    {
        hud = GameObject.Find("Hud");

        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        timeText = GameObject.Find("TimeText").GetComponent<Text>();
        //armsText = GameObject.Find("ArmsText").GetComponent<Text>();
        //bombText = GameObject.Find("BombText").GetComponent<Text>();

    }

    public void ActivateHud ()
    {
        ShowHud(true);
    }

    public IEnumerator PauseForSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        GlobalRefs.currentStageManager.SetMatchState(StageManager.MatchState.Starting);

    }

    void ShowHud (bool show)
    {
        hud.SetActive(show);
    }

    void ToggleCanvas ()
    {
        ShowHud(!hud.activeSelf);
    }

    public void UpdateHUD()
    {
        UpdateUIScore();
        //UpdateUILives();
        //UpdateUIArms();
        //UpdateUIBomb();
        UpdateUITime();
    }

    public void UpdateUIArms()
    {
        //armsText.text = GlobalRefs.currentStageManager.levelName;
    }

    public void UpdateUIBomb()
    {
        //bombText.text = GlobalRefs.currentStageManager.levelName;
    }

    public void UpdateUILives()
    {
        //livesText.text = "1UP = " + GlobalRefs.currentGameManager.player.currentLives.ToString();
    }

    public void UpdateUIScore()
    {
        scoreText.text = GlobalRefs.currentGameManager.player.currentStageScore.ToString("0000000");
    }

    public void UpdateUITime()
    {
        timeText.text = GlobalRefs.currentStageManager.currentMatchTime.ToString("00");
    }




}
