﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Static class used for saving and consulting managers references.
/// </summary>
public static class GlobalRefs
{
    public static StageManager currentStageManager;
    public static GameManager currentGameManager;
    public static SoundManager currentSoundManager;
    public static UIManager currentUIManager;
}
