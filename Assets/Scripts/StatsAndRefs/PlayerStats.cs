﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player info class. It could be static on not multiplayer (and simplified) version.
/// </summary>
public class PlayerStats
{
    // TODO : Make under getters and setters for antihacking (f.e. steam data injection) 
    public int currentGlobalScore;

    public int currentStageScore;

    public int currentLives;

    public int currentArms;

    public int currentBombs;

    public PlayerStats ()
    {

    }

    public PlayerStats (int globalScore, int stageScore, int lives, int arms, int bombs) // For loading previously saved player
    {
        currentGlobalScore = globalScore;
        currentStageScore = stageScore;
        currentLives = lives;
        currentArms = arms;
        currentBombs = bombs;
    }
}
