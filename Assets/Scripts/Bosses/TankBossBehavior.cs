﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Boss behavior
/// </summary>
public class TankBossBehavior : MonoBehaviour
{
    public int life = 10;
    
    public GameObject projectilePrefab;
    private Transform projectileSpawn;
    private Animator bossAnimator;

    public enum BossState { Idle, Shooting, Dead}
    private BossState bossState;

    public int projectilesPoolSize = 5;
    private GameObject[] projectiles;

    private int currentProjectile = -1;

    private void Awake()
    {
        projectileSpawn = transform.GetChild(0);
        bossAnimator = GetComponent<Animator>();
    }

    void Start ()
    {
        CreateProjectiles();
        SetState(BossState.Idle);
	}
	
    void Animate ()
    {
        bossAnimator.SetBool("idle", bossState == BossState.Idle);
        bossAnimator.SetBool("shooting", bossState == BossState.Shooting);
        bossAnimator.SetBool("dead", bossState == BossState.Dead);
    }

    /// <summary>
    /// Attacking and waiting to pass to idle
    /// </summary>
    void Attack ()
    {
        float secondsToIdle = Random.Range(1.0f, 4.0f);
        StartCoroutine(WaitRandomSeconds(secondsToIdle, false));
    }

    void CheckBossState ()
    {
        Animate();

        switch (bossState)
        {
            case BossState.Idle:
                Idle();
                break;
            case BossState.Shooting:
                Shoot();
                break;
            case BossState.Dead:
                StartCoroutine(EndStage());
                break;
        }
    }

    /// <summary>
    /// Create a projectiles pool
    /// </summary>
    private void CreateProjectiles ()
    {
        projectiles = new GameObject[projectilesPoolSize];

        for (int i = 0; i < projectilesPoolSize; i++)
        {
            projectiles[i] = Instantiate(projectilePrefab, projectileSpawn.position, Quaternion.identity);
        }
    }

    public void DamageEnemy ()
    {
        life--;

        if (life < 1)
        {
            if (bossState != BossState.Dead)
            {
                Dead();
            }
        }
    }

    void Dead ()
    {
        SetState(BossState.Dead);
    }

    /// <summary>
    /// On stage end it returns to main menu: TODO: gameover scene etc.
    /// </summary>
    /// <returns></returns>
    IEnumerator EndStage ()
    {
        yield return new WaitForSeconds(5);

        SceneManager.LoadScene("MainMenu");
    }

    /// <summary>
    /// From idle to attack in randomly time
    /// </summary>
    void Idle ()
    {
        float secondsToAttack = Random.Range(1.0f,4.0f);
        StartCoroutine(WaitRandomSeconds(secondsToAttack, true));
    }

    void NextProjectile ()
    {
        currentProjectile++;
        if (currentProjectile == projectiles.Length)
        {
            currentProjectile = 0;
        }
    }

    void SetState (BossState state)
    {
        bossState = state;

        CheckBossState();
    }

    /// <summary>
    /// Prepare projectile and shoot it.
    /// </summary>
    void Shoot ()
    {
        NextProjectile();
        projectiles[currentProjectile].SetActive(true);
        projectiles[currentProjectile].SendMessage("Shoot", SendMessageOptions.RequireReceiver);
        Attack();
    }

    /// <summary>
    /// Used for state pass with random waiting
    /// </summary>
    /// <param name="seconds"></param>
    /// <param name="toShooting"></param>
    /// <returns></returns>
    IEnumerator WaitRandomSeconds (float seconds, bool toShooting)
    {
        yield return new WaitForSeconds(seconds);

        if (toShooting)
        {
            SetState(BossState.Shooting);
        }
        else
        {
            SetState(BossState.Idle);
        }
    }
}
