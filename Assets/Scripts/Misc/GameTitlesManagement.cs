﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameTitlesManagement : MonoBehaviour
{
    public float initialPauseTime;
    public float titlePauseTime;
    public float fadeinAlphaVelocity;
    public float fadeoutAlphaVelocity;
    public float scalingVelocity;

    private SpriteRenderer zammeStudiosRenderer;
    private SpriteRenderer presentsRenderer;

    private float titleCol;
    private float scalingFactor;
    private Vector2 scaleVector;
    private float cameraSizeFactor;

    private Camera cameraBehave;

    [System.Serializable]
    public struct TitleChar
    {
        public GameObject titleCharSprite;
        public Transform targetTransform;
    }

    [SerializeField]
    public TitleChar[] titleCharsPrefabs;
    private GameObject[] titleChars;

    private SpriteRenderer titleBackground;

    public AudioSource clink;
    public AudioSource pough;

    private void Awake()
    {
        cameraBehave = FindObjectOfType<Camera>();
        titleBackground = GameObject.Find("Title_BG_1").GetComponent<SpriteRenderer>();
    }

    void Start ()
    {
        titleBackground.color = new Color(1,1,1,0);
        titleChars = new GameObject[titleCharsPrefabs.Length];
        StartCoroutine(StudioIntro());
    }

    IEnumerator StudioIntro ()
    {
        // Initial pause
        yield return new WaitForSeconds(initialPauseTime);

        // Initial Fade
        titleCol = 0f;

        while (cameraBehave.backgroundColor.r < 1.0f)
        {
            titleCol += (Time.deltaTime * fadeinAlphaVelocity);
            cameraBehave.backgroundColor = new Color(titleCol, titleCol, titleCol, 0);
            yield return new WaitForEndOfFrame();
        }

        // Chars instancing scaling
        for (int i = 0; i < titleCharsPrefabs.Length; i++)
        {
            titleChars[i] = GameObject.Instantiate(titleCharsPrefabs[i].titleCharSprite);
            titleChars[i].transform.parent = titleCharsPrefabs[i].targetTransform;
            titleChars[i].transform.localPosition = Vector2.zero;


            scalingFactor = 1.0f;
            titleChars[i].transform.localScale = Vector2.one;

            while (titleChars[i].transform.localScale.x > 0.101f)
            {
                scalingFactor -= (Time.deltaTime * scalingVelocity);
                if (scalingFactor < 0.1f)
                {
                    scalingFactor = 0.1f;
                }
                titleChars[i].transform.localScale = new Vector2(scalingFactor, scalingFactor);
                titleChars[i].transform.localPosition = Vector2.zero;
                yield return new WaitForEndOfFrame();
            }

            clink.Play();

        }

        // Background Fade
        titleCol = 0f;

        while (titleBackground.color.a < 1.0f)
        {
            titleCol += (Time.deltaTime * fadeinAlphaVelocity);
            titleBackground.color = new Color(1,1,1,titleCol);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(titlePauseTime);

        pough.Play();

        // Final Fade
        titleCol = 1f;
        cameraSizeFactor = 1.0f;

        while (cameraBehave.backgroundColor.r > 0.0f)
        {
            titleCol -= (Time.deltaTime * fadeinAlphaVelocity);
            cameraBehave.backgroundColor = new Color(titleCol, titleCol, titleCol, 0);
            cameraBehave.orthographicSize = titleCol;
            yield return new WaitForEndOfFrame();
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
