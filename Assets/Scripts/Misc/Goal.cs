﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Arrive to end of stage
/// </summary>
public class Goal : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            StartCoroutine(GlobalRefs.currentStageManager.StageCleared());
        }
    }
}
