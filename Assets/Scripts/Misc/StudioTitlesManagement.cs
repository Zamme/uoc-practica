﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StudioTitlesManagement : MonoBehaviour
{
    public float initialPauseTime;
    public float titlePauseTime;
    public float fadeinAlphaVelocity;
    public float fadeoutAlphaVelocity;

    private SpriteRenderer zammeStudiosRenderer;
    private SpriteRenderer presentsRenderer;

    private float titleAlpha;

    private void Awake()
    {
        zammeStudiosRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
        presentsRenderer = transform.GetChild(1).GetComponent<SpriteRenderer>();
    }
    void Start ()
    {
        zammeStudiosRenderer.color = new Color(1,1,1,0);
        presentsRenderer.color = new Color(1, 1, 1, 0);

        StartCoroutine(StudioIntro());
    }

    IEnumerator StudioIntro ()
    {
        yield return new WaitForSeconds(initialPauseTime);

        titleAlpha = 0f;

        while (zammeStudiosRenderer.color.a < 1.0f)
        {
            titleAlpha += (Time.deltaTime * fadeinAlphaVelocity);
            zammeStudiosRenderer.color = new Color(1, 1, 1, titleAlpha);
            yield return new WaitForEndOfFrame();
        }

        titleAlpha = 0f;

        while (presentsRenderer.color.a < 1.0f)
        {
            titleAlpha += (Time.deltaTime * fadeinAlphaVelocity);
            presentsRenderer.color = new Color(1, 1, 1, titleAlpha);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(titlePauseTime);

        titleAlpha = 1.0f;

        while (presentsRenderer.color.a > 0.0f)
        {
            titleAlpha -= (Time.deltaTime * fadeinAlphaVelocity);
            zammeStudiosRenderer.color = new Color(1, 1, 1, titleAlpha);
            presentsRenderer.color = new Color(1, 1, 1, titleAlpha);
            yield return new WaitForEndOfFrame();
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
