﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Score bonus manager. All score bonus included. Pending to add the other type bonuses.
/// </summary>
public class ScoreBonus : MonoBehaviour
{
    [System.Serializable]
    public enum ScoreBonusQuantity { Fifty, Hundred, TwoHundred, FourHundred, FiveHundred, EightHundred, Thousand }

    [System.Serializable]
    public enum ScoreBonusType { MushroomKill, EmptyBlock, Coin, Flower, KoopaKill, ReMushroom, Mushroom, TurtleKill, Star }

    [System.Serializable]
    public struct ScoreEffect
    {
        [SerializeField]
        public ScoreBonusQuantity scoreBonusQuantity;
        [SerializeField]
        public ScoreBonusType scoreType;
        [SerializeField]
        public GameObject scoreBonusEffect;
    }

    /// <summary>
    /// Score bonus effects depot.
    /// </summary>
    [SerializeField]
    public ScoreEffect[] scoreEffects;

    public int GetScoreEffectIndex(ScoreBonusType bonusType)
    {
        int iEffect = -1;
        bool found = false;

        while ((iEffect < scoreEffects.Length) && !found)
        {
            iEffect++;
            if (scoreEffects[iEffect].scoreType == bonusType)
            {
                found = true;
            }
        }

        return iEffect;
    }

    /// <summary>
    /// Score bonus translation. Pending to ease all the system.
    /// </summary>
    /// <param name="sBQ"></param>
    /// <returns></returns>
    public static int TranslateQuantity (ScoreBonusQuantity sBQ)
    {
        int quantity = 0;

        switch (sBQ)
        {
            case ScoreBonusQuantity.Fifty:
                quantity = 50;
                break;
            case ScoreBonusQuantity.Hundred:
                quantity = 100;
                break;
            case ScoreBonusQuantity.TwoHundred:
                quantity = 200;
                break;
            case ScoreBonusQuantity.FourHundred:
                quantity = 400;
                break;
            case ScoreBonusQuantity.FiveHundred:
                quantity = 500;
                break;
            case ScoreBonusQuantity.EightHundred:
                quantity = 800;
                break;
            case ScoreBonusQuantity.Thousand:
                quantity = 1000;
                break;
        }

        return quantity;
    }
}
