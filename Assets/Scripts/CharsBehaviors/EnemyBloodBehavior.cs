﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBloodBehavior : MonoBehaviour
{
    private Animator bloodAnimator;

    private void Start ()
    {
        bloodAnimator = GetComponent<Animator>();
    }

    public void StopBlood ()
    {
        bloodAnimator.SetBool("damage", false);
    }
}
