﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Goombas enemy class
/// </summary>
public class Arabian1Behavior : EnemyBehavior
{
    protected override void Start()
    {
        base.Start();

        // Set bonus type
        //bonusType = ScoreBonus.ScoreBonusType.MushroomKill;
    }

    /// <summary>
    /// Basic 2D attacking player system
    /// </summary>
    void ChasePlayer()
    {
        if (!GlobalRefs.currentStageManager.GetCurrentPlayer())
        {
            return;
        }

        if (GlobalRefs.currentStageManager.GetCurrentPlayerTransform().position.x < transform.position.x)
        {
            // Go left
            enemyRigidbody.velocity = new Vector2(-speed, enemyRigidbody.velocity.y);
        }
        else
        {
            // Go Right
            enemyRigidbody.velocity = new Vector2(speed, enemyRigidbody.velocity.y);
        }
    }

    protected override void UsualBehave()
    {
        base.UsualBehave();

        ChasePlayer();
    }
}
