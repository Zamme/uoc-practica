﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTorsoBehavior : MonoBehaviour
{
    public void DamageEnemiesWithKnife()
    {
        GlobalRefs.currentStageManager.GetPlayerBehavior().DamageEnemiesWithKnife();
    }

}
