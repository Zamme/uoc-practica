﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NextGenSprites;
using UnityEngine.SceneManagement;

/// <summary>
/// Human player behavior
/// </summary>
public class PlayerBehavior : MonoBehaviour
{
    private int currentWeapon;

    public Collider2D marcoCollider;

    // For disabling damaging events pause.
    public float noDamageTime = 2.0f;

    // For disabling damaging on star event.
    public float starInvulnerableTime = 10.0f;
    public Vector4 normalHSBC;
    private Vector4 currentHSBC;
    public float hsbcChangeTime = 1.0f;

    // Shoots
    public GameObject shootSpawnPrefab;
    public Vector4 playerGunnerHSBC;
    private GameObject shootSpawnObject;
    private GunBehavior gunBehavior;
    public Vector3 gunPosition;

    // For mario dead animation time purposes.
    public float marioDeathMotionTime = 2.0f;
    public float marioDeathMotionVelocityMultiplier = 2.0f;

    // For player motion customization.
    public float playerVelocityMultiplier;
    public float playerJumpingMultiplier;

    // All "ground" layers mask.
    public LayerMask groundMask;

    public LayerMask destructableBlocksMask;

    public enum MotionState { Idle, Walking, Falling, Paused, Dead}
    public MotionState motionState;

    private bool jumping;
    private bool crouching;
    private bool shooting;
    private bool lookingLeft; // Player's 2D direction

    private float distToGround; // Player's distance to ground

    /// <summary>
    /// "Super" state. Pending to convert to a more protected type.
    /// </summary>
    public bool superMario;

    private bool marioGunner;

    // Is Mario touching the ground?
    private bool onGround;

    private Rigidbody2D playerRigidbody;

    private Animator[] playerAnimators;

    private SpriteRenderer[] playerRenderers;
    private Material playerMaterial;

    private Collider2D playerCollider;

    //private FireballSpawn playerGun;

    private float horizontalAxis;
    private float verticalAxis;
    private bool jumpButton;
    private bool fire1Button;

    private RaycastHit2D[] headHits;

    private bool noDamagePause = false;

    /// <summary>
    /// So close to enemy for knife attack
    /// </summary>
    public List<GameObject> enemiesAtKnifeDistance;
    private AudioSource kniveAudioSource;

    void Start ()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnimators = GetComponentsInChildren<Animator>();
        playerRenderers = GetComponentsInChildren<SpriteRenderer>();
        playerCollider = GetComponent<Collider2D>();

        kniveAudioSource = GetComponent<AudioSource>();

        CreateShootSpawn();

        InitPlayerProps();
    }
	
    public void Animate ()
    {
        foreach (Animator anim in playerAnimators)
        {
            anim.SetFloat("horizontal_velocity", Mathf.Abs(playerRigidbody.velocity.x));
            anim.SetBool("jumping", jumping);
            anim.SetBool("knife", enemiesAtKnifeDistance.Count > 0);
            anim.SetBool("dead", motionState == MotionState.Dead);
            anim.SetBool("falling", motionState == MotionState.Falling);
            anim.SetInteger("weapon", currentWeapon);
            anim.SetBool("crouching", crouching);
            anim.SetFloat("vertical_velocity", playerRigidbody.velocity.y);
            anim.SetBool("shooting", shooting);
        }

        FlipController();
    }

    /// <summary>
    /// T function for sending messages with all param types to animator.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="animParamName"></param>
    /// <param name="paramValue"></param>
    public void Animate<T> (string animParamName, T paramValue)
    {
        if (paramValue is int)
        {
            foreach (Animator anim in playerAnimators)
            {
                anim.SetInteger(animParamName, Convert.ToInt32(paramValue));
            }
        }
        else if (paramValue is bool)
        {
            foreach (Animator anim in playerAnimators)
            {
                anim.SetBool(animParamName, Convert.ToBoolean(paramValue));
            }
        }
        else if (paramValue is float)
        {
            foreach (Animator anim in playerAnimators)
            {
                anim.SetFloat(animParamName, Convert.ToSingle(paramValue));
            }
        }
    }

    void CreateShootSpawn ()
    {
        shootSpawnObject = Instantiate(shootSpawnPrefab, transform);
        shootSpawnObject.transform.localPosition = new Vector3(0.1f, 0.1f, 0);
        gunBehavior = shootSpawnObject.GetComponent<GunBehavior>();
    }

    void Crouch (bool crouch)
    {
        crouching = crouch;
    }

    public void DamageEnemiesWithKnife ()
    {
        for (int iEnemy = 0; iEnemy < enemiesAtKnifeDistance.Count; iEnemy++)
        {
            enemiesAtKnifeDistance[iEnemy].SendMessage("DamageEnemy", false, SendMessageOptions.DontRequireReceiver);
        }
    }

    /// <summary>
    /// If in "Super" state then disable it and start a no damage pause to avoiding almost same time damage events.
    /// </summary>
    public void DamagePlayer ()
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.Damage);

        DeadPlayer();
    }

    /// <summary>
    /// Deactivate physics and start player dead animation.
    /// </summary>
    public void DeadPlayer ()
    {
        GlobalRefs.currentGameManager.player.currentLives--;
        GlobalRefs.currentSoundManager.PauseMusic(true);
        PausePlayer(true);
        playerRigidbody.velocity = Vector2.zero;
        playerRigidbody.bodyType = RigidbodyType2D.Kinematic;
        GlobalRefs.currentStageManager.PlayerDead();
        SetMotionState(MotionState.Dead);
        playerAnimators[0].gameObject.SetActive(false);
        playerAnimators[1].SetBool("dead", true);
        StartCoroutine(DeadPlayerPost());
    }

    IEnumerator DeadPlayerPost ()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("MainMenu");
    }

    /// <summary>
    /// Picking ribbon.
    /// </summary>
    /// <returns></returns>
    IEnumerator Descending (Vector3 startPosition, Vector3 endPosition)
    {
        float descendingVelocity = 2.0f;
        float yPosition = transform.position.y;

        while (transform.position.y > endPosition.y)
        {
            yPosition -= (descendingVelocity * Time.deltaTime);
            transform.position = new Vector3(transform.position.x, yPosition, transform.position.z);

            yield return new WaitForEndOfFrame();
        }

        // Reactivate
        // TODO: UGLY HACK, PENDING TO FIX
        playerRigidbody.bodyType = RigidbodyType2D.Dynamic;
        SetMotionState(MotionState.Paused);
        Animate("jumping", true);
        Jump(0.5f, 5);
        yield return new WaitForSeconds(0.5f);
        SetMotionState(MotionState.Idle);
    }

    public void DisableAnimators ()
    {
        for (int iAnimator = 0; iAnimator < playerAnimators.Length; iAnimator++)
        {
            playerAnimators[iAnimator].enabled = false;
        }
    }

    void DisablePlayerColliders ()
    {
        marcoCollider.enabled = false;
    }

    void FlipController ()
    {
        foreach (SpriteRenderer renderer in playerRenderers)
        {
            renderer.flipX = lookingLeft;
        }

    }

    /// <summary>
    /// Multiplatform target. Android version could be a good idea.
    /// </summary>
    void GetInput () // Conditional compilation for the input method
    {
#if UNITY_EDITOR
        horizontalAxis = Input.GetAxis("Horizontal");
        jumpButton = Input.GetButtonDown("Jump");
        fire1Button = Input.GetButton("Fire1");
        verticalAxis = Input.GetAxis("Vertical");

#elif UNITY_STANDALONE_WIN
        horizontalAxis = Input.GetAxis("Horizontal");
        jumpButton = Input.GetButtonDown("Jump");
        fireButton = Input.GetButtonDown("Fire1");

#elif UNITY_ANDROID
        Debug.Log("Unity Android");

#else
        Debug.Log("Any other platform");
#endif

    }

    /// <summary>
    /// Player arrives to the end of the stage.
    /// </summary>
    public void GoalAchieved ()
    {
        ShowPlayer(false);
        SetMotionState(MotionState.Paused);
        StartCoroutine(GlobalRefs.currentStageManager.StageCleared());
    }

    void InitPlayerProps ()
    {
        //SetMarioCollider(true);

        currentWeapon = 0;

        onGround = false;

        SetMotionState(MotionState.Idle);

        distToGround = playerCollider.bounds.extents.y;

        enemiesAtKnifeDistance = new List<GameObject>();

    }

    bool IsGrounded()
    {
        return Physics2D.Raycast(transform.position, -Vector2.up, distToGround + 0.01f, groundMask);
    }

    public bool IsLookingLeft ()
    {
        return lookingLeft;
    }

    void Jump ()
    {
        jumping = true;
        //PlayJumpSound();
        playerRigidbody.AddForce(new Vector2(0f, playerJumpingMultiplier), ForceMode2D.Impulse);
    }

    /// <summary>
    /// Alternative jump method for manual calls.
    /// </summary>
    /// <param name="jumpForce"></param>
    void Jump (float jumpForce)
    {
        jumping = true;
        playerRigidbody.AddForce(new Vector2(0f, playerJumpingMultiplier * jumpForce), ForceMode2D.Impulse);
    }

    void Jump(float jumpForce, float forward)
    {
        playerRigidbody.AddForce(new Vector2(forward, playerJumpingMultiplier * jumpForce), ForceMode2D.Impulse);
    }

    /// <summary>
    /// It manages the player motion and state machine.
    /// </summary>
    void Move ()
    {
        playerRigidbody.velocity = new Vector3(horizontalAxis * playerVelocityMultiplier, playerRigidbody.velocity.y, 0f);

        if (onGround)
        {
            if (playerRigidbody.velocity.x > 0f)
            {
                lookingLeft = false;
                SetMotionState(MotionState.Walking);
            }
            else if (playerRigidbody.velocity.x < 0f)
            {
                lookingLeft = true;
                SetMotionState(MotionState.Walking);
            }
            else
            {
                 SetMotionState(MotionState.Idle);
            }

            if (jumpButton)
            {
                Jump();
            }
            else if (jumping)
            {
                jumping = false;
            }

            Crouch(verticalAxis < 0.0);
        }
        else
        {
            //SetMotionState(MotionState.Jumping);
        }

        SetGunPosition();
        Shoot(fire1Button);
    }

    /// <summary>
    /// Centralized game objects interactuation to avoid sensors coding dispersion and a better optimization. ("thinking too much" time) 
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D (Collision2D collision)
    {
        if ((motionState != MotionState.Paused) && (motionState != MotionState.Dead))
        {
            switch (collision.gameObject.layer)
            {
                case 17: // Enemies
                    if (transform.position.y > collision.collider.bounds.center.y)
                    {
                        collision.gameObject.SendMessage("DamageEnemy", noDamagePause, SendMessageOptions.RequireReceiver);
                        Jump(0.5f);
                    }
                    else
                    {
                        if (!noDamagePause)
                        {
                            DamagePlayer();
                        }
                        else
                        {
                            collision.gameObject.SendMessage("DamageEnemy", noDamagePause, SendMessageOptions.RequireReceiver);
                        }
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// Centralize all area triggers inside player behavior
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.gameObject.layer == 13)
        {
            if (!enemiesAtKnifeDistance.Contains(collision.gameObject))
            {
                enemiesAtKnifeDistance.Add(collision.gameObject);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 13)
        {
            enemiesAtKnifeDistance.Remove(collision.gameObject);
        }
    }

    /// <summary>
    /// Pause moment for player post-damaged times. Avoids re-damage.
    /// </summary>
    /// <returns></returns>
    IEnumerator NoDamagePauseTimer (float time)
    {
        noDamagePause = true;
        yield return new WaitForSeconds(time);

        noDamagePause = false;
    }

    public void PausePlayer (bool pause)
    {
        if (pause)
        {
            SetMotionState(MotionState.Paused);
        }
        else
        {
            SetMotionState(MotionState.Idle);
        }
    }

    void PlayJumpSound ()
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.Jump);
    }

    /// <summary>
    /// Reset player position when player deads.
    /// </summary>
    public void ResetPlayer ()
    {
        transform.position = GlobalRefs.currentStageManager.spawnPoints[0].transform.position;
        PausePlayer(false);
        playerRigidbody.velocity = Vector2.zero;
        playerRigidbody.bodyType = RigidbodyType2D.Dynamic;
        playerAnimators[0].gameObject.SetActive(true);
    }

    private void SetGunPosition ()
    {
        if (lookingLeft)
        {
            shootSpawnObject.transform.localPosition = new Vector3(-gunPosition.x, gunPosition.y, gunPosition.z);
            shootSpawnObject.transform.rotation = Quaternion.Euler(0f, -90.0f, 0f);
        }
        else
        {
            shootSpawnObject.transform.localPosition = gunPosition;
            shootSpawnObject.transform.rotation = Quaternion.Euler(0f, 90.0f, 0f);
        }
    }

    /// <summary>
    /// Player animation and state machine
    /// </summary>
    /// <param name="ms"></param>
    void SetMotionState (MotionState ms)
    {
        motionState = ms;
    }

    /// <summary>
    /// Shooting system. It can be by a gun or by the knife
    /// </summary>
    /// <param name="shoot"></param>
    private void Shoot (bool shoot)
    {
        shooting = shoot;

        if (shoot)
        {
            if (enemiesAtKnifeDistance.Count < 1)
            {
                gunBehavior.Shoot(true);
            }
            else
            {
                if (!kniveAudioSource.isPlaying)
                {
                    kniveAudioSource.Play();
                }
            }
        }
        else
        {
            gunBehavior.Shoot(false);
        }
    }

    private void ShowPlayer (bool show)
    {
        foreach (SpriteRenderer renderer in playerRenderers)
        {
            renderer.enabled = show;
        }
    }

    private void FixedUpdate()
    {
        onGround = IsGrounded();
    }

    void Update ()
    {
        GetInput();

        if ((motionState != MotionState.Paused) && (motionState != MotionState.Dead))
        {
            Move();
            Animate();
        }

    }
}
