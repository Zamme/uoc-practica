﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enemy master class. All types of enemies inherit from this.
/// </summary>
public class EnemyBehavior : MonoBehaviour
{
    public enum DificultyLevel { Easy, Medium, Hard}
    public DificultyLevel dificultyLevel = DificultyLevel.Easy;

    public float speed = 1.0f;
    public int lives = 5;
    public float deathTimePause = 3.0f;
    public float reboundImpulse = 5.0f;
    
    /// <summary>
    /// Enemy radar
    /// </summary>
    public Collider2D attackingTrigger;

    public Collider2D enemyCollider;

    public enum EnemyState { Enabled, Disabled, Dead, Paused, Attacking}
    public EnemyState enemyState;

    private int scoreEffectIndex;

    private int dificultyProbability;

    protected Rigidbody2D enemyRigidbody;
    protected Animator enemyAnimator;

    protected ScoreBonus.ScoreBonusType bonusType;

    protected Animator bloodEffectAnimator;

    protected AudioSource deadAudioSource;

    protected virtual void Start()
    {
        enemyRigidbody = GetComponent<Rigidbody2D>();
        enemyAnimator = GetComponent<Animator>();
        bloodEffectAnimator = transform.GetChild(0).GetComponent<Animator>();
        deadAudioSource = GetComponent<AudioSource>();

        // All enemies disabled until player enabling
        EnableEnemy(false);
    }

    private void Animate ()
    {
        enemyAnimator.SetBool("walking", enemyRigidbody.velocity != Vector2.zero);
        enemyAnimator.SetBool("attacking", enemyState == EnemyState.Attacking);
    }

    private void Attack ()
    {
        Animate();
    }

    /// <summary>
    /// Enemy state machine
    /// </summary>
    private void CheckState ()
    {
        switch (enemyState)
        {
            case EnemyState.Enabled:
                UsualBehave();
                break;
            case EnemyState.Disabled:
                DisableEnemy();
                break;
            case EnemyState.Dead:
                gameObject.SetActive(false);
                break;
            case EnemyState.Paused:
                DisableEnemy();
                break;
            case EnemyState.Attacking:
                Attack();
                break;
        }
    }

    /// <summary>
    /// Enemy damaged. Prepared for more than one strike possible cases.
    /// </summary>
    public void DamageEnemy (bool deadByShot)
    {
        bloodEffectAnimator.SetBool("damage", true);

        lives--;

        if (lives < 1)
        {
            enemyRigidbody.velocity = Vector2.zero;
            enemyRigidbody.bodyType = RigidbodyType2D.Kinematic;
            enemyCollider.enabled = false;

            KillEnemy(deadByShot);
        }
    }

    public void DamagePlayer ()
    {
        GlobalRefs.currentStageManager.GetPlayerBehavior().DamagePlayer();
    }

    /// <summary>
    /// Enemy death by gun.
    /// </summary>
    /// <returns></returns>
    IEnumerator DeadByPistol ()
    {
        deadAudioSource.Play();
        enemyAnimator.SetBool("dead", true);
        yield return new WaitForSeconds(deathTimePause);

        SetEnemyState(EnemyState.Dead);
    }

    /// <summary>
    /// Enemy dead by knife.
    /// </summary>
    /// <returns></returns>
    IEnumerator DeadByKnife()
    {
        deadAudioSource.Play();
        enemyAnimator.SetBool("dead", true);
        yield return new WaitForSeconds(deathTimePause);

        SetEnemyState(EnemyState.Dead);
    }

    private void DisableEnemy ()
    {
        enemyRigidbody.velocity = Vector2.zero;
    }

    public void EnableEnemy (bool enable)
    {
        if (enable)
        {
            SetEnemyState(EnemyState.Enabled);
        }
        else
        {
            SetEnemyState(EnemyState.Disabled);
        }
    }

    private void EnableChassingTrigger (bool enable)
    {
        attackingTrigger.enabled = enable;
    }

    /// <summary>
    /// Kill enemy procedure; disable collider, disable possible motion, report bonus to stage manager, and death animation
    /// </summary>
    protected virtual void KillEnemy (bool deadByShot)
    {
        EnableEnemy(false);
        enemyRigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
        enemyCollider.enabled = false;
        GlobalRefs.currentStageManager.ReportBonus(bonusType, transform.position);

        if (deadByShot)
        {
            StartCoroutine(DeadByKnife());
        }
        else
        {
            StartCoroutine(DeadByPistol());
        }
    }

    private void OnBecameInvisible()
    {
        SetEnemyState(EnemyState.Disabled);
    }

    private void OnBecameVisible()
    {
        SetEnemyState(EnemyState.Enabled);
    }

    /// <summary>
    /// Touched by projectile.
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D (Collision2D collision)
    {
        if (enemyState != EnemyState.Dead)
        {
            if (collision.gameObject.tag == "PlayerPistolProjectile")
            {
                DamageEnemy(true);
                collision.gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Detects player approach and enables enemy behavior.
    /// </summary>
    /// <param name="col"></param>
    private void OnTriggerEnter2D (Collider2D col)
    {
        switch (col.tag)
        {
            case "Player":
                // EnableChassingTrigger(false); TODO: PENDING TO FIX!!!
                if (enemyState == EnemyState.Enabled)
                {
                    SetEnemyState(EnemyState.Attacking);
                }
                break;

        }

    }
    
    /// <summary>
    /// Enemy behavior is disabled when player moves away from
    /// </summary>
    /// <param name="col"></param>
    private void OnTriggerExit2D (Collider2D col)
    {
        if (col.tag == "Player")
        {
            if (enemyState == EnemyState.Enabled)
            {
                EnableEnemy(false);
            }
         }
    }
    
    public void PauseEnemy ()
    {
        SetEnemyState(EnemyState.Paused);
    }

    public void ResumeEnemy ()
    {
        SetEnemyState(EnemyState.Disabled);
    }

    /// <summary>
    /// Enemy state machine setter.
    /// </summary>
    /// <param name="state"></param>
    private void SetEnemyState (EnemyState state)
    {
        enemyState = state;
        CheckState();
    }

    /// <summary>
    /// Basic difficulty level manager for enemies.
    /// </summary>
    private void SetDificultyProbability()
    {
        // Random probability
        switch (dificultyLevel)
        {
            case DificultyLevel.Easy:
                dificultyProbability = 25;
                break;
            case DificultyLevel.Medium:
                dificultyProbability = 50;
                break;
            case DificultyLevel.Hard:
                dificultyProbability = 75;
                break;
        }

    }

    /// <summary>
    /// Usual enemy behave. It just chases player.
    /// </summary>
    protected virtual void UsualBehave ()
    {
        if (Random.Range(0, 100) > dificultyProbability)
        {
            // TODO
            return;
        }
    }

    private void FixedUpdate ()
    {
        if ((enemyState == EnemyState.Enabled) && (GlobalRefs.currentStageManager.GetCurrentPlayer() != null))
        {
            UsualBehave();
            Animate();
        }
    }
}
