﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Metal Slug type camera. It inherits from CameraBehavior.
/// </summary>
public class MissionCamera : CameraBehavior
{
    public float midScreenCageLimit = 0.4f;
    public float dampTime = 0.0f; // Damping effect
    private Vector3 velocity = Vector3.zero;

    private Transform target; // Player transform
    private Camera smoothCamera;

    // Colliders to avoid player (and other characters) exits from camera view.
    private GameObject[] cameraBoundaries;

    private Vector3 point; // Player->Viewport position
    private Vector3 delta; // Player->Desired position
    private Vector3 destination; // Camera destination point

    private Vector3 startCameraPosition; // Initial stage camera position

    void Start()
    {
        GetCameraComponent();
        GetCameraBoundaries();

        startCameraPosition = transform.position;
    }

    public override void StartCamera ()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");

        if (player != null)
        {
            target = player.transform;
            EnableCameraBoundaries(true);
        }
        else
        {
            Debug.LogError("Camera target not found!");
        }
    }

    public override void DisableCameraMotion ()
    {
        target = null;
    }

    public void EnableCameraBoundaries (bool enable)
    {
        foreach (GameObject bound in cameraBoundaries)
        {
            bound.SetActive(enable);
        }
    }

    void GetCameraBoundaries ()
    {
        cameraBoundaries = new GameObject[transform.childCount];

        for (int iChild = 0; iChild < transform.childCount; iChild++)
        {
            cameraBoundaries[iChild] = transform.GetChild(iChild).gameObject;
        }
    }

    void GetCameraComponent ()
    {
        smoothCamera = GetComponent<Camera>();

        if (!smoothCamera)
        {
            Debug.LogError("Camera component not found!");
        }
    }

    /// <summary>
    /// Resets the camera position after player dead event.
    /// </summary>
    public override void ResetCamera ()
    {
        SetResetingCamera(true);
        EnableCameraBoundaries(false);

        float xPosition;
        if (target.position.x < startCameraPosition.x)
        {
            xPosition = startCameraPosition.x;
        }
        else
        {
            xPosition = target.position.x;
        }
        transform.position = new Vector3(xPosition, transform.position.y, transform.position.z);

        EnableCameraBoundaries(true);
        SetResetingCamera(false);
    }

    /// <summary>
    /// Property for avoiding involuntary camera motion.
    /// </summary>
    /// <param name="reseting"></param>
    void SetResetingCamera (bool reseting)
    {
        resetingCamera = reseting;
    }

    /// <summary>
    /// Camera motion better at LateUpdate time?
    /// </summary>
    void LateUpdate()
    {
        if (target)
        {
            point = smoothCamera.WorldToViewportPoint(target.position);
            delta = target.position - smoothCamera.ViewportToWorldPoint(new Vector3(midScreenCageLimit, point.y, point.z));

            // No return back
            if ((delta.x > 0f) || resetingCamera)
            {
                destination = transform.position + delta;

                // Damping effect
                transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
            }
        }

    }
}
