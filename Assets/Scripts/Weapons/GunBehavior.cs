﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Gun using particle system
/// </summary>
public class GunBehavior : MonoBehaviour
{
    private ParticleSystem ps;
    private AudioSource audioSource;

    private void Start()
    {
        ps = GetComponent<ParticleSystem>();
        audioSource = GetComponent<AudioSource>();
    }

    private void OnParticleCollision(GameObject other)
    {
        other.SendMessage("DamageEnemy", false);
    }

    public void Shoot (bool shoot)
    {
        if (shoot)
        {
            if (!ps.isPlaying)
            {
                audioSource.Play();
                ps.Play();
            }
        }
        else
        {
            if (ps.isPlaying)
            {
                audioSource.Stop();
                ps.Stop();
            }
        }
    }
}
