﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Boss projectile behavior
/// </summary>
public class TankProjectile : MonoBehaviour
{
    Rigidbody2D projectileRigidbody;
    private Vector3 initialPosition;

    private void Awake()
    {
        projectileRigidbody = GetComponent<Rigidbody2D>();
    }

    void Start ()
    {
        initialPosition = transform.position;
        gameObject.SetActive(false);
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.SendMessage("DamagePlayer", SendMessageOptions.DontRequireReceiver);
            gameObject.SetActive(false);
        }
        else if (collision.gameObject.layer == 19) // Deactivate on collision with limit
        {
            gameObject.SetActive(false);
        }

    }

    /// <summary>
    /// Reset position and randomly direction shooting
    /// </summary>
    public void Shoot ()
    {
        transform.position = initialPosition;
        float randomForceX = Random.Range(1.0f, 3.0f);
        float randomForceY = Random.Range(1.0f, 2.0f);
        projectileRigidbody.AddForce(new Vector2(-randomForceX, randomForceY), ForceMode2D.Impulse);
    }
}
